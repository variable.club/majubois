<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Thumb implements FilterInterface
{
    public function applyFilter(Image $image)
    {
      $image->resize(300, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      });
      $image->encode('jpg', 60);
      return $image;
    }
}
