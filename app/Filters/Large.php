<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Large implements FilterInterface
{
    public function applyFilter(Image $image)
    {
      $image->resize(1600, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      });
      $image->encode('jpg', 70);
      return $image;
    }
}
