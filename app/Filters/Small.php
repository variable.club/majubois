<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Small implements FilterInterface
{
    public function applyFilter(Image $image)
    {
      $image->resize(800, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      });
      $image->encode('jpg', 70);
      return $image;
    }
}
