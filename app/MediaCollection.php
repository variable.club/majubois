<?php
namespace App;


class MediaCollection{

  /** @var string */
  public $name = '';
  public $title = '';

  public function __construct(string $name, string $title){
    $this->name = $name;
    $this->title = $title;
  }

  public static function create($name, $title){
    return new static($name, $title);
  }

  public function singleFile(): self{
    $this->singleFile = true;
    return $this;
  }

  public function canAddText(): self{
    $this->canAddText = true;
    return $this;
  }
}
