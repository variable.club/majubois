<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Exception;
use Illuminate\Filesystem\Filesystem;
use Schema;
use Symfony\Component\Console\Helper\SymfonyQuestionHelper;
use Illuminate\Support\Str;

class AdminVariableInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admintool:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Admint Variable configuration';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
      $this->line('');
      $this->line('');
      $this->line('');
      $this->line('         Admin variable');
      $this->line('');
      $this->line('                    °°');
      $this->line('');
      $this->laravel['env'] = 'local';
      // Get the .env file content
      $contents = $this->getKeyFile();
      // Ask for database name
      $this->info('Setting up database...');
      $projectName = $this->ask('Enter the short name of the project', $this->guessDatabaseName());
      $projectFullName = $this->ask('Enter the long name of the project', ucFirst($this->guessDatabaseName()));
      $dbName = $this->ask('Enter the database name', $this->guessDatabaseName());
      $dbUserName = $this->ask('What is the MySQL username ?', 'root');
      $dbPassword = $this->secret('What is the MySQL password ? (type null if empty)', 'false');
      $dbPassword = ($dbPassword !== 'null') ? $dbPassword : '';

      if($this->confirm('Is the website multlingual ? (yes|no)[yes]',true)){
        $isMultilingual = 'true';
        $locales = $this->ask('Enter the languages codes (comma separated)  ?', 'en,fr,nl');
        $locale = $this->ask('Enter the main language code ?', 'en');

      }else{
        $isMultilingual = 'false';
        $locale = $this->ask('Enter the language codes  ?', 'en');
        $locales = $locale;
      }

      // exit();
      // Update DB credentials in .env file.
      $search = [
        '/('.preg_quote('APP_NAME=').')(.*)/',
        '/('.preg_quote('APP_FULLNAME=').')(.*)/',
        '/('.preg_quote('DB_DATABASE=').')(.*)/',
        '/('.preg_quote('DB_USERNAME=').')(.*)/',
        '/('.preg_quote('DB_PASSWORD=').')(.*)/',
        '/('.preg_quote('APP_MULTILINGUAL=').')(.*)/',
        '/('.preg_quote('APP_LOCALES=').')(.*)/',
        '/('.preg_quote('APP_LOCALE=').')(.*)/',
        '/('.preg_quote('APP_FALLBACK_LOCALE=').')(.*)/',
      ];
      $replace = [
          '$1'.$projectName,
          '$1'.'"'.$projectFullName.'"',
          '$1'.$dbName,
          '$1'.$dbUserName,
          '$1'.$dbPassword,
          '$1'.$isMultilingual,
          '$1'.$locales,
          '$1'.$locale,
          '$1'.$locale,
      ];
      $contents = preg_replace($search, $replace, $contents);
      if (!$contents) {
        throw new Exception('Error while writing credentials to .env file.');
      }else{
          // Write to .env
          $this->files->put('.env', $contents);
          // Generate an new app key
          $this->call('key:generate');
          $this->line('Environement file updated');
      }
      $this->line('Creating database…');
      // $this->line($dbPassword);
      // Set DB username and password in config
      $this->laravel['config']['database.connections.mysql.username'] = $dbUserName;
      $this->laravel['config']['database.connections.mysql.password'] = $dbPassword;
      // Clear DB name in config
      unset($this->laravel['config']['database.connections.mysql.database']);
      // Force the new login to be used
      DB::purge();
      // Create database if not exists
      DB::unprepared('CREATE DATABASE IF NOT EXISTS `'.$dbName.'`');
      DB::unprepared('USE `'.$dbName.'`');
      DB::connection()->setDatabaseName($dbName);
      $this->line('°°');
      $this->line('Database created');
      // Migration & seed
      if (Schema::hasTable('migrations')) {
          $this->error('A migrations table was found in database ['.$dbName.'], no migration and seed were done.');
      } else {
          $this->line('Migrating…');
          $this->call('migrate');
          $this->line('Seeding…');
          $this->call('db:seed');
      }
      $this->call('route:clear');
      $this->call('cache:clear');
      $this->call('optimize');
      // Done
      $this->line('');
      $this->line('°°');
      $this->line('Done, thank you.');
      $this->line('');
    }


    /**
     * Guess database name from app folder.
     *
     * @return string
     */

    public function guessDatabaseName(){
        try {
            $segments = array_reverse(explode(DIRECTORY_SEPARATOR, app_path()));
            $name = explode('.', $segments[1])[0];
            $name = Str::slug($name);

            return $name;
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * Get the key file and return its content.
     *
     * @return string
     */
    protected function getKeyFile(){
      return $this->files->exists('.env') ? $this->files->get('.env') : $this->files->get('.env.example');
    }


}
