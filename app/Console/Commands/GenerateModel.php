<?php

namespace App\Console\Commands;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;


class GenerateModel extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:custom-model';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a variable custom model';

    /**
    * The type of class being generated.
    *
    * @var string
    */
    protected $type = 'Job';

    // Location of your custom stub
    protected function getStub()
    {
        $folder = (env('APP_MULTILINGUAL')) ? 'Multilingual' : 'Unilingual';
        return  app_path().'/Console/Commands/Stubs/'. $folder .'/Model.stub';
    }


    // The root location the file should be written to
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }


    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);
        $stub = str_replace(
            ['DummyNamespace', 'DummyRootNamespace', 'NamespacedDummyUserModel', 'DummySingular'],
            [$this->getNamespace($name), $this->rootNamespace(), config('auth.providers.users.model'), Str::plural(strtolower($class))],
            $stub
        );
        return $this;
    }


    /**
    * Get the console command options.
    *
    * @return array
    */
    protected function getOptions()
    {
        return [];
    }


}
