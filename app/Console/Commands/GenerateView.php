<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GenerateView extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:view {view} {admin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new blade template.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $view = strtolower($this->argument('view'));
        $view_plural = Str::plural($view);
        $view_plural_upper = ucfirst($view_plural);
        $is_admin = $this->argument('admin') && $this->argument('admin') == true ? true : false ;
        $folder = (env('APP_MULTILINGUAL')) ? 'Multilingual' : 'Unilingual';
        // Get the stub
        if($is_admin):
            // Generate View Index
            $viewIndex = str_replace('.', '/', $view_plural) . '-index.blade.php';
            $stub = File::get(app_path().'/Console/Commands/Stubs/ViewAdminIndex.stub');
            $path = "resources/views/admin/templates/{$viewIndex}";
            $this->createFile($path, $stub, $view_plural);
            // Generate View Edit
            // TODO: Dynamic preview article in fiorm edit (route relative to model)
            $viewEdit = str_replace('.', '/', $view) . '-edit.blade.php';
            $stub = File::get(app_path().'/Console/Commands/Stubs/'. $folder .'/ViewAdminEdit.stub');
            $path = "resources/views/admin/templates/{$viewEdit}";
            $this->createFile($path, $stub, $view_plural);
            // Create the corresponding admin routes
            $route_path = 'routes/admin.php';
            $route_rows[] = "// $view_plural_upper \n";
            $route_rows[] = "Route::get('".$view_plural."/getdata', '".$view_plural_upper."Controller@getDataTable')->name('".$view_plural.".getdata');\n";
            $route_rows[] = "Route::resource('".$view_plural."', '".$view_plural_upper."Controller');\n";
        else:
            $viewFront = str_replace('.', '/', $view) . '.blade.php';
            $stub = File::get(app_path().'/Console/Commands/Stubs/ViewFront.stub');
            $path = "resources/views/templates/{$viewFront}";
            $this->createFile($path, $stub);
            // Create the corresponding web routes
            $route_path = 'routes/web.php';
            $route_rows[] = "// $view_plural_upper : view \n";
            $route_rows[] = "Route::get('/".$view_plural."/{slug}', '".$view_plural_upper."Controller@show')->name('".$view_plural.".show');\n";
        endif;
        // Create the routes
        $this->createRoutes($route_path, $route_rows);
    }


    /**
     * Create the files
     *
     * @param $path, $stub
     */
    public function createFile($path, $stub, $dummy_name = '')
    {
        if (File::exists($path))
        {
            $this->error("File {$path} already exists!");
            return;
        }
        // SearchReplace
        $stub = str_replace(
            ['DummyNamespace'],
            [$dummy_name],
            $stub
        );

        $this->createDir($path);
        File::put($path, $stub);
        $this->info("File {$path} created.");
    }


    /**
     * Create view directory if not exists.
     *
     * @param $path
     */
    public function createDir($path)
    {
        $dir = dirname($path);
        if (!file_exists($dir))
        {
            mkdir($dir, 0777, true);
        }
    }


    /**
     * Create view directory if not exists.
     *
     * @param $path
     */
    public function createRoutes($path, $rows)
    {
        file_put_contents( base_path($path), $rows, FILE_APPEND );
    }

}
