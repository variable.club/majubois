<?php

// TODO: Make crud generator like

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenerateRessource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admintool:generate-ressource';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate a new resource.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
      $this->line('');
      $this->line('°°');
      $this->line('Admin variable: generate a new ressource');
      $this->line('');
      $resource = $this->ask('What is the singular name of the ressource ?');
      // generate some useful names
      list($resource, $table, $resource_plural) = [
          $r = ucfirst(str_singular($resource)),
          snake_case(str_plural($r)),
          str_plural($resource)
      ];
      $is_multilingual = env('APP_MULTILINGUAL');
      if($is_multilingual){
        $this->line('Creating a multilingual ressource…');
      }else{
        $this->line('Creating an uninlingual ressource…');
      }
      // Create a default model
      $this->line('Generate a default model…');
      $this->call('make:custom-model', ['name' => $resource]);
      if($is_multilingual){
        // Create a default model translation
        $this->line('Generate model translation…');
        $this->call('make:custom-model-translation', ['name' => $resource.'Translation']);
      }
      // Create a default migration
      $this->line('Generate migration…');
      $this->call('make:custom-migration', ['name' => 'create_' . $table . '_table', '--table' => $table]);
      if($is_multilingual){
        // Create the translation migration
        $this->line('Generate migration translation…');
        $this->call('make:custom-migration-translation', ['name' => 'create_' . $table . '_translations_table', '--table' => $table]);
      }
      // Create the front controller
      $this->line('Generate the front controller…');
      $this->call('make:custom-controller', ['name' => $resource_plural . 'Controller', '--table' => $table]);
      // Create the admin controller
      $this->line('Generate the admin controller…');
      $this->call('make:custom-admin-controller', ['name' => $resource_plural . 'Controller', '--table' => $table]);
      // Create the front view
      $this->line('Generate the front view…');
      $this->call('make:view', ['view' => $resource, 'admin' => false]);
      // Create the admin views
      $this->line('Generate the admin views…');
      $this->call('make:view', ['view' => $resource, 'admin' => true]);

      $this->line('');
      $this->line('°°');
      $this->line('New resource generated successfully');
      $this->line('');
      $this->info('');
    }
}
