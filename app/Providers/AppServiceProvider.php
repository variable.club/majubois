<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Session;
use Blade;
use App;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
      Schema::defaultStringLength(191);
      // If many languages > Set the app locale according to the URL
      $locales = config('translatable.locales');
      if(count($locales) > 1 ){
        if (in_array($request->segment(1), $locales)) {
          App::setLocale(request()->segment(1));
        }else{
          App::setLocale(config('app.fallback_locale'));
        }
      }
      // Use laravel default markdown parser
      Blade::directive('markdown', function ($expression) {
        return "<?php echo (new Parsedown)->text($expression); ?>";
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
