<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Media extends Model{
  use Translatable;
  protected $casts = ['custom_properties' => 'array'];
  protected $table = 'medias';
  public $translatedAttributes = ['description'];
  protected $fillable = ['mediatable_type', 'mediatable_id', 'name', 'file_name', 'collection_name', 'width', 'height', 'order', 'mime_type', 'custom_properties', 'focal'];


  /**
  * Upload a file and create a media
  *
  * @param  file  $file
  * @return \Illuminate\Http\Response
  */

  public static function createFromFile($file, $media_id = null){
    list($width, $height) = getimagesize($file);
    $file_name = $file->getClientOriginalName();
    $orig_name = pathinfo($file_name, PATHINFO_FILENAME);
    $extension = $file->getClientOriginalExtension();
    $name = time() .'-'. str_slug($orig_name).'.'.$extension;
    // File upload
    $path = $file->storeAs('public/medias', $name);
    // Media store
    if(empty($media_id)):
      $media = New media;
    else:
      $media = Media::findOrFail($media_id);
    endif;
    $media->file_name       = $name;
    $media->name            = $orig_name;
    $media->mime_type       = $file->getMimeType();
    $media->width           = $width;
    $media->height          = $height;
    $media->mediatable_id   = 0;
    $media->mediatable_type = 'App\Article';
    $media->collection_name = null;
    $media->save();
    return $media;
  }


  /**
  * Add to collection
  *
  * @param  string  $collection_name
  * @return \Illuminate\Http\Response
  */

  public function addToCollection($collection_name){
    $this->collection_name = $collection_name;
    $this->save();
    return $this;
  }

  /**
 * Supprime le media et le fichier associé
 * @param  int  $id
 */

  public static function deleteMediaFile($id){
    $media = Media::find($id);
    if($media){
      Storage::delete('public/medias/'.$media->file_name);
      $media->delete();
    }
  }


  /**
  * Get all of the owning mediatable models.
  */

  public function mediatable(){
    return $this->morphTo();
  }

}
