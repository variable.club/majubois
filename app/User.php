<?php

// IDEA: Use settings https://github.com/glorand/laravel-model-settings?ref=laravelnews#usage

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
  use Notifiable;
  use HasRoles;   // Spatie laravel-permission
  protected $fillable = ['name', 'email', 'password'];
  // The attributes that should be hidden for arrays.
  protected $hidden = ['password', 'remember_token'];

  /**
   * Hash the password field
   *
   */

  public function setPasswordAttribute($password){
    if (!empty($password)){
      // $this->attributes['password'] = bcrypt($password);
      $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }
  }
}
