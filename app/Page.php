<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Http\Traits\MediaTrait;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Page extends Model{
  use MediaTrait;
  use Translatable;
  protected $table = 'pages';
  public $translatedAttributes = ['title', 'intro', 'text', 'slug'];
  protected $fillable = ['created_at', 'order', 'published', 'parent_id'];


  /**
   * Construct : default Locale
   *
   */

  public function __construct(array $attributes = []){
    parent::__construct($attributes);
    $this->addMediaCollection('une', __('admin.featured_image'))->singleFile();
  }

  /**
   * Get children articles
   *
   */

  public function children(){
    return $this->hasMany('App\Page', 'parent_id')
                ->orderBy('order', 'asc');
  }


  /**
   * Get children articles
   *
   */

  public function parent(){
    return $this->belongsTo('App\Page', 'parent_id');
  }

  /**
   * Réécrit la date d'update
   * @param string  $value (date)
	 *
   */
  public function getUpdatedAtAttribute($value){
    if(!empty($value)){
      Carbon::setLocale(config('app.locale'));
      $date = Carbon::parse($value)->diffForHumans();
    }else{
      $date = "";
    }
    return $date;
  }



  /**
   * Set default parent_id
   *
   */

  public function setParentIdAttribute($parent_id){
    if (empty($parent_id)){
      $this->attributes['parent_id'] = 0;
    }else{
      $this->attributes['parent_id'] = $parent_id;
    }
  }


  /**
   * Concact mdoel + title for related dropdown
   * @param date  $date
   *
   */

   public function getModelTitleAttribute(){
     $data = get_class().', '.$this->id;
     return $data;
   }
}
