<?php
use Carbon\Carbon;

if (! function_exists('formatedDate')) {
    function formatedDate($date){
      if(!empty($date)){
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
      }else{
        return null;
      }
    }
}


/**
* Build navigation from Admin routes
*
* @return string
*/

if (! function_exists('buildAdminNav')) {
  function buildAdminNav(){
    $i = 0;
    foreach (\Route::getRoutes()->getIterator() as $route){
      if(
        strpos($route->getName(), 'admin') !== false &&
        strpos($route->getName(), 'index') !== false &&
        $route->getName() != 'admin.users.index' &&
        $route->getName() != 'admin.roles.index' &&
        $route->getName() != 'admin.permissions.index' &&
        $route->getName() != 'admin.settings.index' &&
        $route->getName() != 'admin.taxonomies.index' &&
        $route->getName() != 'admin.medias.index'
      ){
        $route_name = $route->getName();
        $title = explode('.',$route_name);
        $routes[$i]['name'] = $route_name;
        $routes[$i]['title'] = $title[1];
        $i++;
      }
    }
    return $routes;
  }
}
