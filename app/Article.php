<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Http\Traits\MediaTrait;
use App\Http\Traits\TaxonomyTrait;
use App\MediaCollection;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Article extends Model{
  use MediaTrait;
  use TaxonomyTrait;
  use Translatable;
  protected $table = 'articles';
  public $translatedAttributes = ['title', 'intro', 'text', 'slug'];
  protected $fillable = ['created_at', 'order', 'parent_id', 'published', 'color'];


  /**
  * Construct : default Locale
  *
  */

  public function __construct(array $attributes = []){
    parent::__construct($attributes);
    $this->addMediaCollection('une', __('admin.featured_image'))->singleFile();
    // $this->addMediaCollection('gallery', __('admin.gallery'))->canAddText();
  }


  /**
  * Retourne l'id de l'article sur base du slug
  * @param string  $title
  *
  */

  public static function getSlugFromId($id){
    $article = Article::find($id);
    if($article){
      return $article->slug;
    }
  }


  /**
   * Réécrit la date d'update
   * @param string  $value (date)
	 *
   */
   // TODO: Réécrire la date pour toutes les ressources
  public function getUpdatedAtAttribute($value){
    if(!empty($value)){
      Carbon::setLocale(config('app.locale'));
      $date = Carbon::parse($value)->diffForHumans();
    }else{
      $date = "";
    }
    return $date;
  }


  /**
   * Retourne une liste de catégories associées à l'article
   * Nécessaire pour le dropdown select
   *
   */

   public function getCategoriesAttribute() {
     return $this->taxonomies->pluck('id')->all();
 	 }

  /**
   * Retourne une liste des tags associées à l'article
   * Nécessaire pour le dropdown select
   *
   */

  public function getTagsAttribute() {
    return $this->taxonomies->pluck('id')->all();
  }


  /**
  * Create formated date-time
  */

  public function dateTime($date){
    if(!empty($date)){
      return Carbon::createFromFormat('d.m.Y', $date)->format('Y-m-d');
    }else{
      return null;
    }
  }


  /**
  * Concact model + title for related dropdown
  * @param date  $date
  *
  */

  public function getModelTitleAttribute(){
    $data = get_class().', '.$this->id;
    return $data;
  }


  /**
  * Loop through models that has medias related model
  * Define in config/admin.php
  *
  * @return Articles collection
  */

  static function listAll(){
    $media_models = config('admin.media_models');
    $articles = [];
    if($media_models){
     foreach($media_models as $model){
       $model_name = str_plural(str_replace('App\\','', $model));
       $articles[$model_name] = $model::all()->pluck('title', 'model_title')->toArray();
     }
    }
    return $articles;
  }
}
