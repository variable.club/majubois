<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use App\Setting;
use Illuminate\Support\Facades\Cache;

class ArticlesController extends FrontController{


  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct(){
    $this->minutes = 48*60; // 48h
    parent::__construct();
  }

  /**
   * Show the article
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function show($slug){

    $article = Cache::remember('article'.' '.$slug, $this->minutes, function() use($slug){
      return $article = Article::whereTranslation('slug', $slug)->first();
    });
    if($article):
      $data = array(
        'page_class' => 'article'.' '.$article->slug,
        'page_title' => $article->title . ' - '. config('app.fullname'),
        'page_description' => str_limit($article->text, 300),
        'page_name' => 'articles.index',
      );
      return view('templates/article', compact('article', 'data'));
    else:
      abort(404);
    endif;

  }

}
