<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Page;
use App\Article;
use App\Media;
use App\Setting;
use Illuminate\Support\Facades\Cache;

class HomeController extends FrontController{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
      $this->minutes = 48*60; // 48h
      parent::__construct();
    }


    /**
     * Show the homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $settings = Setting::find([1,2]);
      $website_description = $settings[0]->content;
      $data = array(
        'page_class' => 'body page wrapper',
        'page_title' => config('app.fullname'),
        'page_description' => $website_description,
        'page_name' => 'home',
      );
      // Intro
      $intro = Cache::remember('homepage-intro', $this->minutes, function() {
        return  $intro = Page::find(1);
      });
      $articles = Cache::remember('homepage', $this->minutes, function(){
        return $articles = Article::where('published', 1)
                          ->orderBy('order')
                          ->get();
      });
      return response()
            ->view('templates/home', compact('articles', 'intro', 'data'));
    }
}
