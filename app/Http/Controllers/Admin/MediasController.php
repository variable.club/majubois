<?php

namespace App\Http\Controllers\Admin;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\MediaRequest;
use App\Article;
use App\Media;
use Illuminate\Support\Facades\Cache;

class MediasController extends AdminMediasController {

  public function __construct(){
    $this->table_type = 'medias';
    $this->middleware(['auth', 'permissions'])->except('index');
    parent::__construct();
  }


  /**
   * List all medias by parent
   *
   * @return \Illuminate\Http\Response
   */

  public function index(){
    $data = array(
      'page_class' => 'medias tools',
      'page_title' => 'Medias',
      'page_id'    => 'medias',
      'table_type' => $this->table_type,
    );
    $medias = Media::orderBy('created_at', 'desc')->get();
    return view('admin/templates/medias-index', compact('medias', 'data'));
  }


  /**
  * Get articles for datatables (ajax)
  *
  * @return \Illuminate\Http\Response
  */

  public function getDataTable(){
    return \DataTables::of(Media::get())
                        ->addColumn('img', function ($article) {
                          return '/imagecache/thumb/' . $article->file_name;
                        })
                        ->addColumn('action', function ($article) {
                          return '<a href="' . route('admin.medias.edit', $article->id) . '" class="link">' . __('admin.edit') . '</a>';
                        })
                        ->make(true);
  }


  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */

  public function edit($id){
    $articles = Article::listAll();
    $media = Media::findOrFail($id);
    $data = array(
      'page_class' => 'media tools',
      'page_title' => 'Media edit',
      'page_id'    => 'medias',
    );
    return view('admin/templates/medias-edit',  compact('media', 'data', 'articles'));
  }


  /**
  * Show the form for creating a new resource.
  *
  * @param  string  $parent_slug
  * @return \Illuminate\Http\Response
  */

  public function create(){
    $media = new Media;
    $data = array(
      'page_class' => 'media tools',
      'page_title' => 'Media create',
      'page_id'    => 'medias'
    );
    $articles = Article::listAll();
    return view('admin/templates/medias-edit', compact('media', 'data', 'articles'));
  }


  /**
  * Update the specified resource
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  */

  public function update(Media $media, MediaRequest $request){
    // // Save article
    return $this->saveObject($media, $request);
  }


  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */

  public function store(MediaRequest $request){
    // Create article
    return $this->saveObject(null, $request);
  }


  /**
  * Return article's medias
  *
  * @param  string  $media_type
  * @param  string  $mediatable_type
  * @param  int  $article_id
  * @return \Json\Response
  */

  public function getMediasFromArticle($model_type, $model_id, $collection_name){
    $class = $this->getClass($model_type);
    $article = $class::findOrFail($model_id);
    if($article->medias):
      $medias = $article->medias->where('collection_name', $collection_name);
    endif;
    return response()->json([
      'success' => true,
      'medias' => $medias,
    ]);
  }


  /**
  * Store media related to an article
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  string  $mediatable_type
  * @param  int  $article_id
  * @return JSON\Response
  */

  public function ajaxStore(MediaRequest $request, $model_type, $model_id){

    $files = $request->file('medias');
    // Get article
    $class = $this->getClass($model_type);
    $article = $class::findOrFail($model_id);
    if($files){
      // If file(s) : upload media(s) & create from file
      if($article && $request->single_file){
        $article->deleteAllFromCollection($request->collection_name);
      }
      foreach ($files as $file) {
        // Upload file(s) & create media(s)
        $media = Media::createFromFile($file);
        if($request->collection_name){
          $media->addToCollection($request->collection_name);
        }
        // Link article to the media
        if($article){
          $article->medias()->save($media);
        }
      }
      // Flush the cache
      Cache::flush();
      return response()->json([
        'success'          => true,
        'collection_name' => $request->collection_name,
      ]);
    }else{
      // If text : add text only media (no upload)
      $request['mediatable_type'] = $model_type;
      $request['mediatable_id'] = $model_id;
      $request['mime_type'] = 'text';
      // dd($request->all());
      $media = $this->createObject($request);
      $article->medias()->save($media);
      // Flush the cache
      Cache::flush();
      return response()->json([
        'success'          => true,
        'collection_name' => $request->collection_name,
      ]);
    }
  }


  /**
  * Return article
  *
  * @param  int  $media_id
  * @return \Json\Response
  */

  public function ajaxGet($id){
    if($id):
      $media = Media::findOrFail($id);
      return response()->json([
        'success' => true,
        'media' => $media,
      ]);
    else:
      abort(404);
    endif;

  }

  /**
  * Store media related to an article
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  string  $mediatable_type
  * @param  int  $article_id
  * @return JSON\Response
  */

  static function storeText(MediaRequest $request, $model_type, $model_id){
    dd('storeText');
    // $media = new Media();
    // $media->mediatable_type = $this->getClass($model_type);
    // $media->mediatable_id = $model_id;
    // $media->name = 'text';
    // $media->file_name = '';
    // // dd($media->mediatable_type);
    // $media->save();

    // Get article
    // $class = $this->getClass($model_type);
    // $article = $class::findOrFail($model_id);
    $request['mediatable_type'] = 'App\Article';
    $request['mediatable_id'] = $model_id;
    $request['mime_type'] = 'text/plain';
    $request['name'] = 'text';
    $request['file_name'] = '';
    // $media->collection_name = null;
// dd($request->all());
    // Create media
    $media = $this->createObject($request);

    // if($files){
    //   if($article && $request->single_file){
    //     $article->deleteAllFromCollection($request->collection_name);
    //   }
    //   foreach ($files as $file) {
    //     // Upload file(s) & create media(s)
    //     $media = Media::createFromFile($file);
    //     if($request->collection_name){
    //       $media->addToCollection($request->collection_name);
    //     }
    //     // Link article to the media
    //     if($article){
    //       $article->medias()->save($media);
    //     }
    //   }
    //   // Flush the cache
    //   Cache::flush();
    //   return response()->json([
    //     'success'          => true,
    //     'collection_name' => $request->collection_name,
    //   ]);
    // }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */

  public function destroy(Media $media){
    return $this->destroyObject($media);
  }

  /**
   * Quick Destroy (ajax)
   *
   * @param $model
   * @return Json
   */

  public function quickDestroy(Request $request){
    Media::deleteMediaFile($request->media_id);
    return response()->json([
      'success'         => true,
      'collection_name' => $request->collection_name,
    ]);
  }


  /**
  * Reorder medais related to an article
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */

  public function reorder(Request $request, $model_type, $article_id, $collection_name){
    $class = $this->getClass($model_type);
    $article = $class::findOrFail($article_id);
    $media_id  = $request->mediaId;
    $new_order = $request->newOrder;
    $v = 1;
    if($article->medias):
      $medias = $article->medias->where('collection_name', $collection_name);
    endif;
    if(isset($medias)){
     $v = 0;
     // loop in related medias
     foreach ($medias as $media) {
       $media = Media::findOrFail($media->id);
       if($v == $new_order){$v++;}
       if($media->id == $media_id){
         $media->order = $new_order;
       }else{
         $media->order = $v;
         $v++;
       }
       $media->timestamps = false;
       // Update Media with new order
       $media->update();
     }
    }
    return response()->json([
     'status' => 'success',
    ]);
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */

  public function ajaxMediaUpdate(Request $request, $mediatable_type){
    $id = $request->media_id;
    $request['focal'] = array('top' => $request->focal_top, 'left' => $request->focal_left);
    unset($request['focal_top']);
    unset($request['focal_left']);
    $media = Media::findOrFail($id);
    $media->update($request->all());
    return response()->json([
     'success'         => true,
     'media_id'        => $id,
     'collection_name' => $media->collection_name,
    ]);
  }

  /**
  * Upload de fichier simple (pour les champs texte)
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */

  public function fileUpload(MediaRequest $request){
    $file = $request->file;
    $validator = \Validator::make($request->all(), [
      'file' => 'required|image|mimes:jpeg,jpg,png,gif,pdf,mp4|max:3000'
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors()->first('file');
      return response()->json(['message' => $errors], 404);
    }else{
    // dd($request->all());
    $file_name = $file->getClientOriginalName();
    $orig_name = pathinfo($file_name, PATHINFO_FILENAME);
    $extension = $file->getClientOriginalExtension();
    $name = time() .'-'. str_slug($orig_name).'.'.$extension;
    $path = $file->storeAs('public/medias', $name);
    if($extension == 'pdf'){
      $file_url = '/medias/'.$name;
    }else{
      $file_url = '/imagecache/large/'.$name;
    }
    $file->move('medias', $name);
    return response()->json([
      'status'    => 'success',
      'image_url' => $file_url
    ]);
  }

  }

  /**
  * Sanitize media name
  *
  * @param  file
  * @return string
  */

  public function fileName($file){
    $file_name = $file->getClientOriginalName();
    $orig_name = pathinfo($file_name, PATHINFO_FILENAME);
    $extension = $file->getClientOriginalExtension();
    $name = str_slug($orig_name).'.'.$extension;
    return $name;
  }


  /**
  * Link Related Article id exists
  *
  * @param  \Illuminate\Http\Request  $request
  * @return true
  */

  static function linkRelatedArticle($request){
    if(!empty($request->associated_article)):
      $article = explode(',', $request->associated_article);
      $article_model = $article[0];
      $article_id = $article[1];
      if(!empty($article_model) && !empty($article_id) && $article_id != 'null'):
        $request['model'] = $article_model;
        $request['article_id'] = $article_id;
      endif;
    else:
      $request['model'] = null;
      $request['article_id'] = null;
    endif;
    return true;
  }


}
