<?php

namespace App\Http\Controllers\Admin;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use App\Page;
use App\Media;
use App\Taxonomy;
use Route;
use DB;
use Carbon\Carbon;
use Lang;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;


class AdminController extends Controller{

  /**
   * Model name
   *
   * @var string
   */

  protected $model = '';


  /**
   * Construct
   *
   * @var string
   */

  public function __construct(){
    $this->minutes = 48*60; // 48h
    $this->middleware(['auth', 'permissions'])->except('index');
    $this->model = $this->getModel();
    // $admin_primary_nav = Cache::remember('admin_primary_nav', $this->minutes, function () {
    //   return $this->buildNavigation();
    // });
    // View::share('admin_primary_nav', $admin_primary_nav);
  }


  /**
   * Update // save an object
   *
   * @param $model
   * @param $request
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */

  public function saveObject($model, $request, $taxonomies = false, $parent_id = null){
    $class = get_class($model);
    $article = $class::findOrFail($model->id);
    if($request['created_at']){
      $request['created_at'] = Carbon::createFromFormat('d.m.Y', $request->created_at )->format('Y-m-d H:i:s');
    }
    // Checkbox update
    if($model->published):
      $request['published'] = (($request['published']) ? 1 : 0);
    endif;
    // Taxonomies
    if(!empty($taxonomies)):
      Taxonomy::manageRelationships($taxonomies, $request, $article->id, $class);
    endif;
    // Do update
    $article->update($request->all());
    // Flush the cache
    Cache::flush();
    // Redirect
    session()->flash('flash_message', __('admin.updated'));
    if(isset($request['finish'])){
      if(isset($request->parent_id)):
        return redirect()->route('admin.' . snake_case($this->model) . '.index', $request->parent_id);
      else:
        return redirect()->route('admin.' . snake_case($this->model) . '.index');
      endif;
    }else{
      return redirect()->route('admin.' . snake_case($this->model) . '.edit', $request->id);
    }
  }


  /**
   * Update // save an object
   *
   * @param $model
   * @param $request
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */

  public function createObject($class, $request, $return_type = 'redirect', $taxonomies = false){

    // Increment order of all articles
    if(isset($request->order)):
      if(empty($request->parent_id)):
        $incremented = $class::increment('order');
      else:
        $incremented = $class::where('parent_id', $request->parent_id)->increment('order');
      endif;
    endif;
    // Article create
    $article = $class::create($request->all());
    // Flush the cache
    Cache::flush();
    // Taxonomies
    if(!empty($taxonomies)):
      Taxonomy::manageRelationships($taxonomies, $request, $article->id, $class);
    endif;
    session()->flash('flash_message', __('admin.created'));
    if($return_type == 'redirect'){
      if(isset($request['finish'])){
        if(isset($request->parent_id)):
          return redirect()->route('admin.' . snake_case($this->model) . '.index', $request->parent_id);
        else:
          return redirect()->route('admin.' . snake_case($this->model) . '.index');
        endif;
      }else{
        return redirect()->route('admin.' . snake_case($this->model) . '.edit', $article->id);
      }
    }else{
      return $article;
    }
  }


  /**
   * Delete
   *
   * @param $model
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */

  public function destroyObject($model, $path = 'index'){
    $class = get_class($model);
    $article = $class::findOrFail($model->id);
    // cascade delete medias
    if(!empty($article->medias)):  foreach ($article->medias as $media):
      Media::deleteMediaFile($media->id);
    endforeach; endif;
    $article -> delete();
    // Flush the cache
    Cache::flush();
    session()->flash('flash_message', __('admin.deleted'));
    return redirect()->route('admin.' . snake_case($this->model) . '.index', $article->parent_id);
  }

  /**
   * Reorder an object
   *
   * @param $class
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
   */

  public function orderObject(Request $request, $mediatable_type){

    $count = 0;
    $class = $this->getClass($mediatable_type);

    if (count($request->json()->all())) {
      $ids = $request->json()->all();
      // Loop through update positions
      foreach($ids as $i => $key){
        $id = $key['id'];
        $position = $key['position'];
        $article = $class::find($id);
        $article->order = $position;
        $article->timestamps = false;
        $article->save();
        $count++;
      }
      $response = 'Articles ordered';
      // Flush the cache
      Cache::flush();
      return response()->json( $response );
    } else {
      $response = 'Nothing to order';
      return response()->json( $response );
    }
  }


  /**
  * Get model name, if isset the model parameter, then get it, if not then get the class name, strip "Controller" out
  *
  * @return string
  */

  protected function getModel(){
    return empty($this->model) ?
      explode('Controller', substr(strrchr(get_class($this), '\\'), 1))[0]  :
      $this->model;
  }


  /**
  * Get the class from table name
  *
  * @return string
  */

  public function getClass($table_name){
    return !empty($table_name) ?
      'App\\' . studly_case(str_singular($table_name)) :
      null;
  }




}
