<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class Language
{
  // Ajoute le suffixe (fallback) de langue  à l'url si pas de suffixe présent
  // https://mydnic.be/post/how-to-build-an-efficient-and-seo-friendly-multilingual-architecture-in-laravel-v2
  public function handle($request, Closure $next)
  {
      $locales = config('translatable.locales');
      // If there is more than one language defined
      if(count($locales) > 1){
        // Check if the first segment matches a language code
        if (!in_array($request->segment(1), $locales)) {
            // Store segments in array
            $segments = $request->segments();
            // Set the default language code as the first segment
            $segments = array_prepend($segments, config('translatable.fallback_locale'));
            // Redirect to the correct url
            return redirect()->to(implode('/', $segments));
        }
      }

      // The request already contains the language code
      return $next($request);
  }

}
