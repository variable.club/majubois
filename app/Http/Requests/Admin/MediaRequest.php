<?php

namespace App\Http\Requests\Admin;
use Illuminate\Foundation\Http\FormRequest;
use Response;

class MediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
      $medias = !is_null($this->input('medias')) ? count($this->input('medias')) : 0;
      foreach(range(0, $medias) as $index) {
        $rules['medias.' . $index] = 'image|mimes:jpeg,jpg,png,gif,pdf,mp4|max:3000';
      }
      return $rules;
      // return [
      //   'name' => 'max:300',
      //   'medias' => 'required|mimes:jpeg,jpg,png,gif,pdf,mp4|max:3000',
      //   // $medias = count($this->input('medias'));
      //   // foreach(range(0, $medias) as $index) {
      //   //   $rules['medias.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
      //   // }
      //   // return $rules;
      // ];
    }


    public function response(array $errors){
      // return response()->json(['error' => $errors]);
      return response()->json([
        'error'       =>  $errors,
        'collection'  => $request->collection_name,
      ]);
    }
}
