<?php
namespace App\Http\Traits;
use App\Taxonomy;


trait TaxonomyTrait {

       /**
    * Get all of the taxonomies for the post.
    */

    public function taxonomies(){
      return $this->morphToMany('App\Taxonomy', 'taxonomyable');
    }


   /**
   * Get the category
   */


   public function cat(){
     return $this->morphToMany('App\Taxonomy', 'taxonomyable')
                 ->where('parent_id', 1);
   }
   

   /**
    * Returns the categories for a select
    *
    */

   public function taxonomiesDropdown($parent_id, $appendEmpty=0){
     if($appendEmpty){
       return Taxonomy::where('parent_id', $parent_id)->get()->pluck('name', 'id')->prepend('', '');
     }else{
       return Taxonomy::where('parent_id', $parent_id)->get()->pluck('name', 'id');
     }
   }
}
