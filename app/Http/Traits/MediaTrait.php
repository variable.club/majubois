<?php
namespace App\Http\Traits;
use App\Media;
use App\MediaCollection;

trait MediaTrait {
  /** @var array */
  public $mediaCollections = [];


  /**
   * Returns medias
   *
   */

   public function medias(){
     return $this->morphMany(Media::class, 'mediatable')
                 ->orderBy('order', 'asc');
   }


  /**
  * Returns medias by collection
  *
  */

  public function getMedias($collection_name = 'une'){
    $medias = $this->morphMany(Media::class, 'mediatable')
                ->where('collection_name', $collection_name)
                ->orderBy('order', 'asc');
    return $medias->get();

  }

  /**
  * Returns medias collection
  *
  */

  public function addMediaCollection(string $name, string $title): MediaCollection{
    $mediaCollection = MediaCollection::create($name, $title);
    $this->mediaCollections[] = $mediaCollection;
    return $mediaCollection;
  }


  /**
  * Delete all medias from collection
  *
  * @param  string  $collection_name
  * @param  string  $collection_name
  * @return \Illuminate\Http\Response
  */

  public function deleteAllFromCollection($collection_name){
    $media = $this->medias->where('collection_name', $collection_name)->first();
    if(!empty($media)):
      Media::deleteMediaFile($media->id);
    endif;
  }
}
