<?php

use Illuminate\Database\Seeder;
use App\Taxonomy;

class Taxonomies extends Seeder
{

      /**
       * Run the database seeds.
       *
       * @return void
       */
      public function run()
      {
        // Taxonomy 1
        $data = [
          'parent_id' => 0,
          'order' => 0,
        ];

        $data = $this->addTranslatedContent($data, 'Categories');
        Taxonomy::create($data);


        // Taxonomy 2
        $data = [
          'parent_id' => 1,
          'order' => 0,
        ];

        $data = $this->addTranslatedContent($data);
        Taxonomy::create($data);

        // Taxonomy::create([
        //   'en'  => ['name' => 'Tags'],
        //   'fr'  => ['name' => 'Tags'],
        //   'parent_id' => 0,
        //   'order' => 1,
        // ]);
        //
        // Taxonomy::create([
        //   'en'  => ['name' => 'Categorie 1'],
        //   'fr'  => ['name' => 'Categorie 1'],
        //   'parent_id' => 1,
        //   'order' => 0,
        // ]);
        //
        // Taxonomy::create([
        //   'en'  => ['name' => 'Tag 01'],
        //   'fr'  => ['name' => 'Tag 01'],
        //   'parent_id' => 2,
        //   'order' => 0,
        // ]);

      }

        /**
         * Create fake content based on locales
         *
         * @return array
         */

        private function addTranslatedContent($data, $name = ''){
          // Get locales
          $locales = config('translatable.locales');
          // Faker create text
          $faker = Faker\Factory::create();
          $name = ($name) ? $name : $faker->unique()->word;
          // Add content by locale
          foreach($locales as $key => $locale) {
              $d = [
                'name' => $name
              ];
              unset($locales[$key]);
              $data[$locale] = $d;
          }
          return $data;
        }

}
