<?php

use Illuminate\Database\Seeder;
use App\Page;

class Pages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
          'parent_id' => 0,
          'published' => 1,
          'order' => 0,
        ];

        $data = $this->addTranslatedContent($data);

        Page::create($data);
    }


    /**
     * Create fake content based on locales
     *
     * @return array
     */

    private function addTranslatedContent($data){
      // Get locales
      $locales = config('translatable.locales');
      // Faker create text
      $faker = Faker\Factory::create();
      // Add content by locale
      foreach($locales as $key => $locale) {
          $d = [
            'title' => $faker->unique()->word,
          ];
          unset($locales[$key]);
          $data[$locale] = $d;
      }
      return $data;
    }
}
