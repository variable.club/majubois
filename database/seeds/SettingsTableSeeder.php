<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $data = [
        'order' => 0,
      ];

      $data = $this->addTranslatedContent($data);

      Setting::create($data);

      // Google Analytics
      Setting::create([
        'name' => 'Google analytics',
        'description' => 'Enter a valid tracking ID. It should look like: UA-XXXXXX-X.',
        'order' => 1,
      ]);

    }


    /**
     * Create fake content based on locales
     *
     * @return array
     */

    private function addTranslatedContent($data){
      // Get locales
      $locales = config('translatable.locales');
      // Faker create text
      $faker = Faker\Factory::create();
      // Add content by locale
      foreach($locales as $key => $locale) {
          if($locale == 'nl'):
            $name = 'Beschrijving van de website';
            $description = 'Beschrijf uw website voor de resultaten van de search engines (max. 320 tekens)';
          elseif($locale == 'fr'):
            $name = 'Description du site';
            $description = 'Une courte description de votre site web pour les moteurs de recherche (320 caractères max.)';
          else:
            $name = 'Website description';
            $description = 'Describe your website for search engines results (max. 320 characters)';
          endif;
          $d = [
            'name' => $name,
            'description' => $description,
            'content' => config('app.name')
          ];
          unset($locales[$key]);
          $data[$locale] = $d;
      }
      return $data;
    }
}
