<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMediaTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('media_translations', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('media_id')->unsigned();
        $table->string('locale')->index();
        $table->text('description')->nullable();
        $table->unique(['media_id','locale']);
        $table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');
      });
      Schema::table('medias', function(Blueprint $table){
        $table->dropColumn('description');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_translations');
    }
}
