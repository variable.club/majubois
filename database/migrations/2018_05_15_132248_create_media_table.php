<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('mediatable');
            $table->string('collection_name')->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('file_name');
            $table->string('mime_type')->nullable();
            $table->text('custom_properties')->nullable();;
            $table->unsignedInteger('order')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->text('focal')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('medias');
    }
}
