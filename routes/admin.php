<?php
// Emails tests
// Route::get('mailable', function () {
//     $user = App\User::find(1);
//     return new App\Mail\AdminNewUser($user);
// });
// Admin specific routes
Route::get('/', ['as' => 'root', 'uses' => 'ArticlesController@index']);
Route::get('users/getdata', 'UsersController@getDataTable')->name('users.getdata');
Route::resource('users', 'UsersController');
Route::get('roles/getdata', 'RoleController@getDataTable')->name('roles.getdata');
Route::resource('roles', 'RoleController');
Route::get('permissions/getdata', 'PermissionController@getDataTable')->name('permissions.getdata');
Route::resource('permissions', 'PermissionController');
Route::resource('roles', 'RoleController');
Route::get('articles/getdata', 'ArticlesController@getDataTable')->name('articles.getdata');
Route::resource('articles', 'ArticlesController');


Route::get('pages/{parent_id}/getdata', 'PagesController@getDataTable')->name('pages.getdata');
Route::resource('pages', 'PagesController', ['except' => ['create', 'index']]);
Route::get('pages/{parent_id}/create', 'PagesController@create')->name('pages.create');
Route::get('pages/{parent_id?}/index', 'PagesController@index')->name('pages.index');
Route::post('pages/reorder/{id}', 'PagesController@reorder')->name('pages.reorder');
Route::resource('settings', 'SettingsController');
//taxonomies
Route::get('taxonomies/getdata', 'TaxonomiesController@getDataTable')->name('taxonomies.getdata');
Route::resource('taxonomies', 'TaxonomiesController', ['except' => ['create']]);
Route::get('taxonomies/create/{parent_id}', 'TaxonomiesController@create')->name('taxonomies.create');
Route::post('taxonomies/reorder/{id}', 'TaxonomiesController@reorder')->name('taxonomies.reorder');
Route::post('{table_type}/reorder', 'AdminController@orderObject')->name('reorder');
// Medias
Route::get('medias/getdata', 'MediasController@getDataTable')->name('medias.getdata');
Route::delete('medias/quickdestroy', 'MediasController@quickDestroy')->name('medias.quickdestroy');
Route::resource('medias', 'MediasController');
Route::get('getMediasFromArticle/{model_type}/{model_id}/{collection_name}', 'MediasController@getMediasFromArticle')->name('medias.article');
Route::post('medias/ajaxStore/{mediatable_type}/{article_id?}', 'MediasController@ajaxStore')->name('medias.ajaxstore');
Route::get('medias/ajaxGet/{id}', 'MediasController@ajaxGet')->name('medias.ajaxget');
Route::post('medias/reorder/{model_type}/{article_id}/{collection_name}', 'MediasController@reorder')->name('medias.reorder');
Route::post('medias/setfocalpoint', 'MediasController@setFocalPoint')->name('medias.setfocal');
// Route::post('medias/get', 'MediasController@getFromArray');
Route::post('medias/AjaxMediaUpdate/{mediatable_type}', 'MediasController@AjaxMediaUpdate')->name('medias.ajaxmediaupdate');
Route::post('fileupload', 'MediasController@fileUpload')->name('fileupload');
// Datatables
Route::get('datatable', 'DataTablesController@datatable');
Route::get('datatable/getArticles', 'DataTablesController@getArticles')->name('datatable/getdata');
