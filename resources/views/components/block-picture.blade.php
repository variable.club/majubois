@if(count($article->getMedias('une')))
  <section class="section picture" style="background-color:{{ $article->color }}">
    <picture>
      @php $media = $article->getMedias('une')->first() @endphp
      @include('components.image', ['src' => $media->file_name, 'alt' => $media->alt])
    </picture>
  </section>
@endif
