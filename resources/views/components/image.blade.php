<img
  data-sizes="auto"
  src="{{ url('/imagecache/small') }}/{{ $src }}"
  data-src="{{ url('/imagecache/medium') }}/{{ $src }}"
  data-srcset="{{ url('/imagecache/small') }}/{{ $src }} 800w,
  {{ url('/imagecache/medium') }}/{{ $src }} 1200w,
  {{ url('/imagecache/large') }}/{{ $src }} 1600w"
  alt="{{ $alt }}"
  class="image-lazy lazyload"
  @if(!empty($focal_left))
    style="object-position:{{ $focal_left }} 50%"
  @endif
   />
