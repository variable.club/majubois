<section class="section text">
  <div class="content-container">
    <h1 class="logo">
      <img src="./assets/images/majubois-logo.svg" alt="Majubois" class="logo__img">
    </h1>
    <address>
      @markdown($text)
    </address>
  </div>
</section>
