@extends('app')

@section('page_title', $data['page_title'])
@section('page_class', $data['page_class'])
@section('page_description', strip_tags($data['page_description']))

@section('content')
  <main class="main-container" id="main-container">
    @if($intro)
      @include('components.block-text', ['text'=> $intro->text])
    @endif

    @if($articles) @foreach ($articles as $article)
      @include('components.block-picture')
    @endforeach @endif
  </main>
@endsection
