<!DOCTYPE html>
<html>
    <head>
        <title>Page not found.</title>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: black;
                display: table;
                font-weight: 100;
                font-family: sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 26px;
                margin-bottom: 40px;
                margin-top: 15px;
            }
            .fantom{
                background-repeat: no-repeat;
                background-size: contain;
                position: absolute;
                box-shadow: 34px 2px 0 0 #000000, 36px 2px 0 0 #000000, 38px 2px 0 0 #000000, 40px 2px 0 0 #000000, 30px 4px 0 0 #000000, 32px 4px 0 0 #000000, 34px 4px 0 0 #ffffff, 36px 4px 0 0 #ffffff, 38px 4px 0 0 #ffffff, 40px 4px 0 0 #ffffff, 42px 4px 0 0 #000000, 44px 4px 0 0 #000000, 28px 6px 0 0 #000000, 30px 6px 0 0 #ffffff, 32px 6px 0 0 #ffffff, 34px 6px 0 0 #ffffff, 36px 6px 0 0 #ffffff, 38px 6px 0 0 #ffffff, 40px 6px 0 0 #ffffff, 42px 6px 0 0 #ffffff, 44px 6px 0 0 #ffffff, 46px 6px 0 0 #000000, 26px 8px 0 0 #000000, 28px 8px 0 0 #ffffff, 30px 8px 0 0 #ffffff, 32px 8px 0 0 #ffffff, 34px 8px 0 0 #ffffff, 36px 8px 0 0 #ffffff, 38px 8px 0 0 #ffffff, 40px 8px 0 0 #ffffff, 42px 8px 0 0 #ffffff, 44px 8px 0 0 #ffffff, 46px 8px 0 0 #ffffff, 48px 8px 0 0 #000000, 24px 10px 0 0 #000000, 26px 10px 0 0 #ffffff, 28px 10px 0 0 #ffffff, 30px 10px 0 0 #ffffff, 32px 10px 0 0 #ffffff, 34px 10px 0 0 #ffffff, 36px 10px 0 0 #ffffff, 38px 10px 0 0 #ffffff, 40px 10px 0 0 #ffffff, 42px 10px 0 0 #ffffff, 44px 10px 0 0 #ffffff, 46px 10px 0 0 #ffffff, 48px 10px 0 0 #ffffff, 50px 10px 0 0 #000000, 24px 12px 0 0 #000000, 26px 12px 0 0 #ffffff, 28px 12px 0 0 #ffffff, 30px 12px 0 0 #ffffff, 32px 12px 0 0 #ffffff, 34px 12px 0 0 #ffffff, 36px 12px 0 0 #ffffff, 38px 12px 0 0 #ffffff, 40px 12px 0 0 #ffffff, 42px 12px 0 0 #ffffff, 44px 12px 0 0 #ffffff, 46px 12px 0 0 #ffffff, 48px 12px 0 0 #ffffff, 50px 12px 0 0 #000000, 24px 14px 0 0 #000000, 26px 14px 0 0 #ffffff, 28px 14px 0 0 #ffffff, 30px 14px 0 0 #ffffff, 32px 14px 0 0 #ffffff, 34px 14px 0 0 #000000, 36px 14px 0 0 #ffffff, 38px 14px 0 0 #ffffff, 40px 14px 0 0 #ffffff, 42px 14px 0 0 #ffffff, 44px 14px 0 0 #000000, 46px 14px 0 0 #ffffff, 48px 14px 0 0 #ffffff, 50px 14px 0 0 #000000, 22px 16px 0 0 #000000, 24px 16px 0 0 #ffffff, 26px 16px 0 0 #ffffff, 28px 16px 0 0 #ffffff, 30px 16px 0 0 #ffffff, 32px 16px 0 0 #ffffff, 34px 16px 0 0 #000000, 36px 16px 0 0 #ffffff, 38px 16px 0 0 #ffffff, 40px 16px 0 0 #ffffff, 42px 16px 0 0 #ffffff, 44px 16px 0 0 #000000, 46px 16px 0 0 #ffffff, 48px 16px 0 0 #ffffff, 50px 16px 0 0 #ffffff, 52px 16px 0 0 #000000, 22px 18px 0 0 #000000, 24px 18px 0 0 #ffffff, 26px 18px 0 0 #ffffff, 28px 18px 0 0 #ffffff, 30px 18px 0 0 #ffffff, 32px 18px 0 0 #ffffff, 34px 18px 0 0 #000000, 36px 18px 0 0 #ffffff, 38px 18px 0 0 #ffffff, 40px 18px 0 0 #ffffff, 42px 18px 0 0 #ffffff, 44px 18px 0 0 #000000, 46px 18px 0 0 #ffffff, 48px 18px 0 0 #ffffff, 50px 18px 0 0 #ffffff, 52px 18px 0 0 #000000, 22px 20px 0 0 #000000, 24px 20px 0 0 #ffffff, 26px 20px 0 0 #ffffff, 28px 20px 0 0 #ffffff, 30px 20px 0 0 #ffffff, 32px 20px 0 0 #ffffff, 34px 20px 0 0 #000000, 36px 20px 0 0 #ffffff, 38px 20px 0 0 #ffffff, 40px 20px 0 0 #ffffff, 42px 20px 0 0 #ffffff, 44px 20px 0 0 #000000, 46px 20px 0 0 #ffffff, 48px 20px 0 0 #ffffff, 50px 20px 0 0 #ffffff, 52px 20px 0 0 #000000, 22px 22px 0 0 #000000, 24px 22px 0 0 #ffffff, 26px 22px 0 0 #ffffff, 28px 22px 0 0 #ffffff, 30px 22px 0 0 #ffffff, 32px 22px 0 0 #ffffff, 34px 22px 0 0 #ffffff, 36px 22px 0 0 #ffffff, 38px 22px 0 0 #ffffff, 40px 22px 0 0 #ffffff, 42px 22px 0 0 #ffffff, 44px 22px 0 0 #ffffff, 46px 22px 0 0 #ffffff, 48px 22px 0 0 #ffffff, 50px 22px 0 0 #ffffff, 52px 22px 0 0 #000000, 22px 24px 0 0 #000000, 24px 24px 0 0 #ffffff, 26px 24px 0 0 #ffffff, 28px 24px 0 0 #ffffff, 30px 24px 0 0 #ffffff, 32px 24px 0 0 #ffffff, 34px 24px 0 0 #ffffff, 36px 24px 0 0 #000000, 38px 24px 0 0 #000000, 40px 24px 0 0 #000000, 42px 24px 0 0 #000000, 44px 24px 0 0 #ffffff, 46px 24px 0 0 #ffffff, 48px 24px 0 0 #ffffff, 50px 24px 0 0 #ffffff, 52px 24px 0 0 #000000, 22px 26px 0 0 #000000, 24px 26px 0 0 #ffffff, 26px 26px 0 0 #ffffff, 28px 26px 0 0 #000000, 30px 26px 0 0 #ffffff, 32px 26px 0 0 #ffffff, 34px 26px 0 0 #ffffff, 36px 26px 0 0 #ffffff, 38px 26px 0 0 #ffffff, 40px 26px 0 0 #ffffff, 42px 26px 0 0 #ffffff, 44px 26px 0 0 #ffffff, 46px 26px 0 0 #ffffff, 48px 26px 0 0 #ffffff, 50px 26px 0 0 #ffffff, 52px 26px 0 0 #000000, 16px 28px 0 0 #000000, 18px 28px 0 0 #000000, 20px 28px 0 0 #000000, 22px 28px 0 0 #ffffff, 24px 28px 0 0 #ffffff, 26px 28px 0 0 #ffffff, 28px 28px 0 0 #ffffff, 30px 28px 0 0 #000000, 32px 28px 0 0 #ffffff, 34px 28px 0 0 #ffffff, 36px 28px 0 0 #ffffff, 38px 28px 0 0 #ffffff, 40px 28px 0 0 #ffffff, 42px 28px 0 0 #ffffff, 44px 28px 0 0 #ffffff, 46px 28px 0 0 #ffffff, 48px 28px 0 0 #ffffff, 50px 28px 0 0 #ffffff, 52px 28px 0 0 #ffffff, 54px 28px 0 0 #000000, 14px 30px 0 0 #000000, 16px 30px 0 0 #ffffff, 18px 30px 0 0 #ffffff, 20px 30px 0 0 #ffffff, 22px 30px 0 0 #ffffff, 24px 30px 0 0 #ffffff, 26px 30px 0 0 #ffffff, 28px 30px 0 0 #ffffff, 30px 30px 0 0 #ffffff, 32px 30px 0 0 #000000, 34px 30px 0 0 #ffffff, 36px 30px 0 0 #ffffff, 38px 30px 0 0 #ffffff, 40px 30px 0 0 #ffffff, 42px 30px 0 0 #ffffff, 44px 30px 0 0 #ffffff, 46px 30px 0 0 #ffffff, 48px 30px 0 0 #ffffff, 50px 30px 0 0 #ffffff, 52px 30px 0 0 #ffffff, 54px 30px 0 0 #ffffff, 56px 30px 0 0 #000000, 12px 32px 0 0 #000000, 14px 32px 0 0 #ffffff, 16px 32px 0 0 #ffffff, 18px 32px 0 0 #ffffff, 20px 32px 0 0 #ffffff, 22px 32px 0 0 #ffffff, 24px 32px 0 0 #ffffff, 26px 32px 0 0 #ffffff, 28px 32px 0 0 #ffffff, 30px 32px 0 0 #ffffff, 32px 32px 0 0 #000000, 34px 32px 0 0 #ffffff, 36px 32px 0 0 #ffffff, 38px 32px 0 0 #ffffff, 40px 32px 0 0 #ffffff, 42px 32px 0 0 #ffffff, 44px 32px 0 0 #ffffff, 46px 32px 0 0 #ffffff, 48px 32px 0 0 #ffffff, 50px 32px 0 0 #ffffff, 52px 32px 0 0 #ffffff, 54px 32px 0 0 #ffffff, 56px 32px 0 0 #000000, 2px 34px 0 0 #000000, 4px 34px 0 0 #000000, 12px 34px 0 0 #000000, 14px 34px 0 0 #ffffff, 16px 34px 0 0 #ffffff, 18px 34px 0 0 #ffffff, 20px 34px 0 0 #ffffff, 22px 34px 0 0 #ffffff, 24px 34px 0 0 #ffffff, 26px 34px 0 0 #000000, 28px 34px 0 0 #ffffff, 30px 34px 0 0 #ffffff, 32px 34px 0 0 #000000, 34px 34px 0 0 #ffffff, 36px 34px 0 0 #ffffff, 38px 34px 0 0 #ffffff, 40px 34px 0 0 #ffffff, 42px 34px 0 0 #ffffff, 44px 34px 0 0 #ffffff, 46px 34px 0 0 #ffffff, 48px 34px 0 0 #000000, 50px 34px 0 0 #ffffff, 52px 34px 0 0 #ffffff, 54px 34px 0 0 #ffffff, 56px 34px 0 0 #000000, 2px 36px 0 0 #000000, 4px 36px 0 0 #ffffff, 6px 36px 0 0 #000000, 8px 36px 0 0 #000000, 10px 36px 0 0 #000000, 12px 36px 0 0 #ffffff, 14px 36px 0 0 #ffffff, 16px 36px 0 0 #ffffff, 18px 36px 0 0 #ffffff, 20px 36px 0 0 #ffffff, 22px 36px 0 0 #ffffff, 24px 36px 0 0 #ffffff, 26px 36px 0 0 #ffffff, 28px 36px 0 0 #000000, 30px 36px 0 0 #000000, 32px 36px 0 0 #ffffff, 34px 36px 0 0 #ffffff, 36px 36px 0 0 #ffffff, 38px 36px 0 0 #ffffff, 40px 36px 0 0 #ffffff, 42px 36px 0 0 #ffffff, 44px 36px 0 0 #ffffff, 46px 36px 0 0 #ffffff, 48px 36px 0 0 #000000, 50px 36px 0 0 #000000, 52px 36px 0 0 #000000, 54px 36px 0 0 #000000, 2px 38px 0 0 #000000, 4px 38px 0 0 #ffffff, 6px 38px 0 0 #ffffff, 8px 38px 0 0 #ffffff, 10px 38px 0 0 #ffffff, 12px 38px 0 0 #ffffff, 14px 38px 0 0 #ffffff, 16px 38px 0 0 #000000, 18px 38px 0 0 #000000, 20px 38px 0 0 #ffffff, 22px 38px 0 0 #ffffff, 24px 38px 0 0 #ffffff, 26px 38px 0 0 #ffffff, 28px 38px 0 0 #ffffff, 30px 38px 0 0 #ffffff, 32px 38px 0 0 #ffffff, 34px 38px 0 0 #ffffff, 36px 38px 0 0 #ffffff, 38px 38px 0 0 #ffffff, 40px 38px 0 0 #ffffff, 42px 38px 0 0 #ffffff, 44px 38px 0 0 #ffffff, 46px 38px 0 0 #000000, 4px 40px 0 0 #000000, 6px 40px 0 0 #ffffff, 8px 40px 0 0 #ffffff, 10px 40px 0 0 #ffffff, 12px 40px 0 0 #ffffff, 14px 40px 0 0 #000000, 20px 40px 0 0 #000000, 22px 40px 0 0 #000000, 24px 40px 0 0 #ffffff, 26px 40px 0 0 #ffffff, 28px 40px 0 0 #ffffff, 30px 40px 0 0 #ffffff, 32px 40px 0 0 #ffffff, 34px 40px 0 0 #ffffff, 36px 40px 0 0 #ffffff, 38px 40px 0 0 #ffffff, 40px 40px 0 0 #ffffff, 42px 40px 0 0 #000000, 44px 40px 0 0 #000000, 6px 42px 0 0 #000000, 8px 42px 0 0 #000000, 10px 42px 0 0 #000000, 12px 42px 0 0 #000000, 24px 42px 0 0 #000000, 26px 42px 0 0 #000000, 28px 42px 0 0 #ffffff, 30px 42px 0 0 #ffffff, 32px 42px 0 0 #ffffff, 34px 42px 0 0 #ffffff, 36px 42px 0 0 #000000, 38px 42px 0 0 #000000, 40px 42px 0 0 #000000, 28px 44px 0 0 #000000, 30px 44px 0 0 #000000, 32px 44px 0 0 #000000, 34px 44px 0 0 #000000;
                height: 2px;
                width: 2px;
                left: 10vh;
                top: 28vh;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="fantom"></div>
                <div class="title">
                    Page not found
                </div>
            </div>
        </div>
    </body>
</html>
