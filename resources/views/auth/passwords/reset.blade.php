@extends('admin.app')
@section('page_class', 'reset_password')
@section('content')

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if ($errors->has('email'))
  <div class="help-block">
    <span>{{ $errors->first('email') }}</span>
  </div>
@endif
@if ($errors->has('password'))
  <div class="help-block">
    <span>{{ $errors->first('password') }}</span>
  </div>
@endif
@if ($errors->has('password_confirmation'))
  <div class="help-block">
    <span>{{ $errors->first('password_confirmation') }}</span>
  </div>
@endif
<div class="panel panel-default">

  <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
      {{ csrf_field() }}
      <div class="form__content">
        <div class="form__body">
          <input type="hidden" name="token" value="{{ $token }}">

          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ $email or old('email') }}" required autofocus>

          </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
          </div>

          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password"required>
          </div>
      </div>
      <button type="submit" class="btn btn-primary">
          Reset Password
      </button>
    </div>
  </form>
</div>

@endsection
