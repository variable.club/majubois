<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @yield('meta')
  <title>@yield('page_title')</title>
  <!-- Styles -->
  <link href="{{ url('/assets/admin/main.css') }}" rel="stylesheet">
</head>
<body  class="admin @yield('page_class')">
  @include('admin.components.flash-message')
  @if (Auth::check())  @include('admin.components.navigation-primary') @endif
  <main class="main-content wrapper">
      @yield('content')
  </main>
  @include('admin.components.footer')
  <!-- JavaScripts -->
  <script type="text/javascript">
  @if(!empty(config('translatable.locales')) && count(config('translatable.locales')) > 1)
    var admin_url = '/{{config('translatable.locales')[0]}}/admin';
  @else
    var admin_url = '/admin';
  @endif
  </script>
  @if (app('env') == 'local' )
    <script src="{{ url('/assets/admin/scripts.js') }}"></script>
  @else
    <script src="{{ url('/assets/admin/scripts.min.js') }}"></script>
  @endif
  <script type="text/javascript">
  $(document).ready(function() {
    var previousPlaceholder;
    $('.main-content').on('mouseover', '.dataTables_filter input', function() {
      if(previousPlaceholder === undefined){
        previousPlaceholder = $(this).attr('placeholder');
      }
      $(this).attr('placeholder','{{__('admin.search')}}');
    });
    $('.main-content').on('mouseout', '.dataTables_filter input', function() {
      $(this).attr('placeholder', previousPlaceholder);
      previousPlaceholder = undefined;
    });
  });
  </script>
  @yield('scripts')
</body>
</html>
