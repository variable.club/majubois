<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('page_title')</title>
  <meta name="description" content="@yield('page_description')">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <?php // TODO: fb:admins ?>
  {{-- <meta property="fb:admins" content=""> --}}
  <?php // TODO: fb:app_id ?>
  {{-- <meta property="fb:app_id" content=""> --}}
  <meta name="apple-mobile-web-app-title" content="{{ config('app.name') }}">
  <meta name="application-name" content="{{ config('app.name') }}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta property="og:title" content="@yield('page_title')">
  <meta property="og:description" content="@yield('page_description')">
  <meta property="og:image" content="@yield('page_image_url')">
  <meta property="og:url" content="{{ Request::url() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/assets/favicon/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <!-- Styles -->
  @if (app('env') == 'local' )
    <link href="{{ url('/assets/front/screen.css') }}" rel="stylesheet">
  @else
    <link href="{{ url('/assets/front/screen.min.css') }}" rel="stylesheet">
  @endif

  @if($google_analytics)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ $google_analytics }}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '{{ $google_analytics }}');
    </script>
  @endif
</head>
<body>
  <div class="@yield('page_class')">
    @yield('content')
    @if (app('env') == 'local' )
      <script src="{{ url('/assets/front/scripts.js') }}"></script>
    @else
      <script src="{{ url('/assets/front/scripts.min.js') }}"></script>
    @endif
  </div>
</body>
</html>
