var gulp          = require('gulp');
var browserSync   = require('browser-sync').create();
var sass          = require('gulp-sass');
var browserify    = require('browserify');
var source        = require('vinyl-source-stream');
var buffer        = require('vinyl-buffer');
var beautify      = require('gulp-beautify');
var rename        = require('gulp-rename');
var sourcemaps    = require('gulp-sourcemaps');
var autoprefixer  = require('gulp-autoprefixer');
var cssmin        = require('gulp-cssmin');
var shell         = require('gulp-shell');
var concat        = require('gulp-concat');
var uglify        = require('gulp-uglify-es').default;
// var babelify      = require('babelify');
var cachebust     = require('gulp-cache-bust');

// Compile sass into CSS & auto-inject into browsers
gulp.task('admin-sass', function() {
  return gulp.src("css/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass({
      style: 'expanded',
      sourceComments: 'normal'
    }))
    // Ajoute des préfixes automatiquement
    .pipe(autoprefixer())
    // Commente le code pour debug
    .pipe(sourcemaps.write())
    // Sauve le fichier dans public/assets
    .pipe(gulp.dest("../../../public/assets/admin"))
    // browserSync
    .pipe(browserSync.stream())
    // Renomme le fichier avec .min
    .pipe(rename({suffix: '.min'}))
    // Compresse le fichier
    .pipe(cssmin())
    // Sauve le fichier dans public/assets
    .pipe(gulp.dest('../../../public/assets/admin'))
});

// process JS files and return the stream.
gulp.task('admin-js', function () {
  return browserify({
    entries: './js/scripts.js',
    debug: true
  })
  // .transform("babelify", { presets: ["env"] })
  .bundle()
  .pipe(source('scripts.js'))
  .pipe(buffer())
  // Indente
  .pipe(beautify({indentSize: 2}))
  .pipe(cachebust({type: 'timestamp'}))
  // Sauve le fichier dans public/assets
  .pipe(gulp.dest("../../../public/assets/admin"))
  // Renomme le fichier avec .min
  .pipe(rename({suffix: '.min'}))
  // Compresse le fichier
  .pipe(uglify())
  // Sauve le fichier compressé dans public/assets
  .pipe(gulp.dest('../../../public/assets/admin'))
});

// reloading browsers
gulp.task('admin-js-watch', gulp.series('admin-js', function (done) {
  browserSync.reload();
  done();
}));

// Static Server + watching scss/html files
gulp.task('serve', gulp.series('admin-sass', function() {
  browserSync.init({
    proxy: "admin-variable.test",
    port: 8080,
    open: false
  });
  gulp.watch("js/**/*.js", gulp.series('admin-js-watch'));
  gulp.watch("css/**/*.scss", gulp.series('admin-sass'));
}));

gulp.task('admin', gulp.series('serve'));
