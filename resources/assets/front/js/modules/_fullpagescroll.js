/*------------------------------------*\
  Full page scroll
\*------------------------------------*/

var $ = require("jquery");
var fullpage = require('fullpage.js');


var updateTitle = function(title) {
  $('#page-title .nav').html(title);
}

global.varFullpage = null;

global.initFullpage = function() {
  varFullpage = new fullpage('#main-container', {
    verticalCentered: false,
    paddingTop: '20px',
    paddingBottom: '20px',
    animateAnchor: false,
    licenseKey: '3A823061-9A394D2E-B61A75EE-FC4A2D50',
    onLeave: function(origin, destination, direction){

    }
  });

}


initFullpage();
