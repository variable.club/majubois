/*------------------------------------*\
  #SCRIPTS
\*------------------------------------*/

var $ = require("jquery");
global.jQuery = require("jquery")

global.menuDisplay = false;
global.detailDisplay = false;

$( document ).ready(function() {
  // modules
  require('./modules/_lazysizes.js');
  require('./modules/_is-touched.js');
  require('./modules/_fullpagescroll.js');
});
